module.exports = {
  root: true,
  extends: ['custom'],
  ignorePatterns: ['README.md', '.eslintrc.js', '/test/**/*.*', 'tsup.config.ts'],
};
