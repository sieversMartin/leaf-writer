export { find as dbpediaFind } from './dbpedia';
export { find as geonamesFind } from './geonames';
export { find as gettyFind } from './getty';
export { find as lgpnFind } from './lgpn';
export { find as viafFind } from './viaf';
export { find as wikidataFind } from './wikidata';
export { find as gndFind } from './gnd';
