import type { DialogLookupType } from '../../js/dialogs/types';
import Entity from '../../js/entities/Entity';
import {
  AuthorityLookupParams,
  AuthorityLookupSettings,
} from '../../overmind/lookups/services/type';

export type NamedEntityType =
  | 'person'
  | 'place'
  | 'organization'
  | 'rs'
  | 'title'
  | 'thing'
  | 'concept';
export type Authority = 'dbpedia' | 'geonames' | 'getty' | 'lgpn' | 'viaf' | 'wikidata' | 'gnd';
export type LookupService = 'LINCS' | 'internal' | 'custom';

export interface AuthorityService {
  enabled: boolean;
  entities: Partial<Record<NamedEntityType, boolean>>;
  find?: (
    params: AuthorityLookupParams,
    settings?: AuthorityLookupSettings,
  ) => Promise<AuthorityLookupResult[] | undefined>;
  readonly id: Authority | string;
  lookupService?: LookupService;
  name?: string;
  priority: number;
  readonly requireAuth?: boolean;
  settings?: AuthorityLookupSettings;
}

export type AuthorityServices = Record<string, AuthorityService>;

export interface AuthorityServiceConfig extends Partial<AuthorityService> {
  id: Authority | string;
}

// export interface LookupsProps {
//   // authorities: { [key: string]: AuthorityService };
//   authorities: Partial<Record<Authority, AuthorityService>>;
// }

// export interface LookupsConfig {
//   authorities: Array<
//     | Authority
//     | [
//         Authority,
//         {
//           config?: {
//             [x: string]: any;
//             username?: string;
//           };
//           enabled?: boolean;
//           entities?: Array<[NamedEntityType, boolean]>;
//         }
//       ]
//   >;
// }

export interface AuthorityLookupResult {
  description?: string;
  id: string;
  name: string;
  repository: Authority;
  logo?: string;
  query: string;
  type: string;
  uri: string;
}

export interface EntityLookupDialogProps {
  onClose?: (response?: EntityLink | Pick<EntityLink, 'query' | 'type'>) => void;
  entry?: Entity;
  open: boolean;
  query?: string;
  type?: DialogLookupType;
}

export interface EntryLink {
  id: string;
  uri: string;
  name: string;
  repository: string;
}

export interface EntityLink {
  id: string;
  name: string;
  properties: {
    lemma: string;
    uri: string;
  };
  query: string;
  repository: string;
  type: string;
  uri: string;
}
